import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { withInfo } from '@storybook/addon-info';

import { Button, Label, Input, LoginForm, NavItem, NavBar } from '../lib/index';
// import LoginForm from '../lib/index';

// storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Button', module)
  .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
  .add('with some emoji', () => <Button onClick={action('clicked')}>😀 😎 👍 💯</Button>)
  .add('Small', () => <Button size="small">Small</Button>)
  .add('Medium', () => <Button size="medium">Medium</Button>)
  .add('Large', () => <Button size="large">Large</Button>)
  .add('Wide', () => <Button size="wide">Wide</Button>)
  .add('Extra Wide', () => <Button size="extraWide">Extra Wide</Button>)
  .add('Full Width', () => <Button size="fullWidth">Full Width</Button>)
  .add('White', () => <Button bgColor="white" fontColor="black">White</Button>)
  .add('Black', () => <Button bgColor="black" fontColor="white">Black</Button>);

storiesOf('Label', module)
  .add('Label Basic', () => <Label>Hello Everybody</Label>)
  .add('Label Small', () => <Label size='small'>Hello Everybody! I'm SMALL!</Label>)
  .add('Label Medium', () => <Label size='medium'>Hello Everybody! I'm MEDIUM!</Label>)
  .add('Label Large', () => <Label size='large'>Hello Everybody! I'm LARGE!</Label>)
  .add('Label Advanced', () => <Label color="black" fontWeight="200" textTransform="lowercase">Just Some Things</Label>);

storiesOf('Input', module)
    .add('Input Basic', () => <Input placeholder="UserName" />)
    .add('Input Styles', () => <Input bgColor="blue" fontColor="white" borderColor="slate" placeholder="UserName" />)
    .add('Input Password', () => <Input type="password" />)
    .add('Input Number', () => <Input type="number" />);
    // .add('Input Email', () => <input type="email" />); // For some reason, none of the styles were applied. So until I figure that mystery out, suffer without an email field.

storiesOf('Login form', module)
    .add('Login Form', withInfo('Login Form Molecule')(()=>
        <LoginForm
            name="login"
            action="#"
            labelUser="Username"
            labelPass="Password"
            />
    ));

storiesOf('NavItem', module)
    .add('Nav Item', () => <NavItem>Hello</NavItem>)
    .add('Blue', () => <NavItem bgColor="blue" fontColor="black">Blue</NavItem>)
    .add('Small', () => <NavItem size="small">Small</NavItem>)
    .add('Medium', () => <NavItem size="medium">Medium</NavItem>)
    .add('Large', () => <NavItem size="large">Large</NavItem>)
    .add('Wide', () => <NavItem size="wide">Wide</NavItem>)
    .add('Extra Wide', () => <NavItem size="extraWide">Extra Wide</NavItem>)
    .add('Full Width', () => <NavItem size="fullWidth">Full Width</NavItem>)
    .add('Button', () => <NavItem><Button>Button</Button></NavItem>);

storiesOf('NavBar', module)
    .add('Navigation Bar', () => <NavBar><NavItem>Hello</NavItem><NavItem><Button>Junk</Button></NavItem></NavBar>);
