import React from 'react';
import Input from '../../elements/input';
import Button from '../../elements/button';
import Label from '../../elements/label';
import styled from 'styled-components'

const Wrapper = styled.div`
    .formElement {
        width: max;
        padding 4px;
    }
    .formButton {
        float: right;
    }
    width: 184px; //Divisible by 8. Who knew?
    padding: 4em;
    `;

const LoginForm = ({ action, submit, labelUser, labelPass, username, passname}) => {
    return (
        <Wrapper className="form-login">
            <form name={name} action={action} onSubmit={submit || null} >
                <div className="formElement">
                    <Label>{labelUser}</Label>
                    <Input type="text" name={username || 'username'} />
                </div>
                <div className="formElement">
                    <Label>{labelPass}</Label>
                    <Input type="password" name={passname || 'password'} />
                </div>
                <div className="formElement formButton">
                    <Button>{submit || 'Log In'}</Button>
                </div>
            </form>
        </Wrapper>
    );
};

export default LoginForm;
