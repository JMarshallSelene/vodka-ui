import React from 'react';
import Input from '../../elements/input';
import NavItem from '../../elements/nav-element';
import * as sizes from '../../styles/sizes';
import * as colors from '../../styles/colors';
import styled from 'styled-components';

const NavBar = styled.div`
    padding: ${({ size }) => sizes[size]['padding']};
    background: ${({ bgColor }) => colors[bgColor]};
    color: ${({ fontColor }) => colors[fontColor]};

    span {
        background: ${({ bgColor }) => colors[bgColor]};
    }
`;

NavBar.defaultProps = {
    size: 'medium',
    bgColor: 'silver',
    fontColor: 'black',
};

export default NavBar;
