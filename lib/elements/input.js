import React from 'react';
import styled from 'styled-components';
import * as colors from '../styles/colors';
import { darken } from 'polished';

const Input = styled.input`
    background-color: ${({ bgColor }) => colors[bgColor]};
    color: ${({ fontColor }) => colors[fontColor]};
    border: 2px solid ${({ borderColor }) => colors[borderColor]};
    border-radius: 4px;
    // placeholder: {
    //     color: ${({ placeholderColor }) => colors[placeholderColor]};
    // }
    `;

Input.defaultProps = {
    bgColor: 'darkSnow',
    fontColor: 'black',
    placeholderColor: 'slate', // Don't really know how to style the placeholder
    borderColor: 'black',
};

export default Input;
