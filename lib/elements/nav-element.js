import React from 'react';
import styled from 'styled-components';
import { lighten, darken } from 'polished';
import * as colors from '../styles/colors';

const navSizes = {
  small: {
    'font-size': '14px',
    'line-height': '30px',
    padding: '0 8px',
  },
  medium: {
    'font-size': '16px',
    'line-height': '40px',
    padding: '0 12px',
  },
  large: {
    'font-size': '18px',
    'line-height': '50px',
    padding: '0 16px',
  },
  wide: {
    'font-size': '16px',
    'line-height': '40px',
    padding: '0 36px',
  },
  extraWide: {
    'font-size': '16px',
    'line-height': '40px',
    padding: '0 72px',
  },
  fullWidth: {
    'font-size': '16px',
    'line-height': '40px',
    padding: '0 8px',
  },
};
function setDisplay({ size }) {
  return size === 'fullWidth' ? 'block' : 'inline-block';
}
function setWidth({ size }) {
  return size === 'fullWidth' ? '100%' : 'initial';
}
const NavItem = styled.span`
    // background: ${({ bgColor }) => colors[bgColor]};
    color: ${({ fontColor }) => colors[fontColor]};
    border-radius:0;
    display :${setDisplay};
    font-size: ${({ size }) => navSizes[size]['font-size']};
    line-height: ${({ size }) => navSizes[size]['line-height']};
    font-weight: 200;
    margin: 8px 0;
    outline: none;
    padding: ${({ size }) => navSizes[size]['padding']};
    transition: all 300ms ease;
    width: ${setWidth};
    &:hover {
        background: ${({ bgColor })  => lighten(0.1, colors[bgColor])};
    }
`;

NavItem.defaultProps = {
    bgColor: 'silver',
    fontColor: 'black',
    size: 'medium',
};

export default NavItem;
